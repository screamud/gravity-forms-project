<?php get_header(); ?>
	<div class="container" id="main">
		<ul class="nav nav-pills nav-justified custom-tabs">
			<li class="active"><a data-toggle="tab" href="#sis">SIS</a></li>
			<li><a data-toggle="tab" href="#lms">LMS</a></li>
			<li><a data-toggle="tab" href="#ldap">LDAP/AD</a></li>
		</ul>

		<div class="tab-content">
			<div id="sis" class="tab-pane fade in active">
				<div class="row">
					<div class="col-md-12 table-form">
						<div class="form-group">
							<?php gravity_form( 1, false, false, false, '', true ); ?>
						</div>
					</div>
				</div>
			</div>
			<div id="lms" class="tab-pane fade">
				<div class="row">
					<div class="col-md-12 table-form">
						<div class="form-group">
							<?php gravity_form( 2, false, false, false, '', true ); ?>
						</div>
					</div>
				</div>
			</div>
			<div id="ldap" class="tab-pane fade ldap">
				<div class="row">
					<div class="col-md-12 footer-block">
						<div class="form-group">
							<?php gravity_form( 3, false, false, false, '', true ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>